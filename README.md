# Gulp build project


The project uses gulp with version 4.0.2  that why you need to have proper gulp-cli version (CLI version: 2.3.0) 
```
$ npm rm -g gulp
$ npm install -g gulp-cli
```

## Getting started
```
$ npm  i
```

## Getting developing
```
$ npm run dev
```

## Project build
```
$ npm run prod
```
