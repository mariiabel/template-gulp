const gulp = require("gulp");

// BUILD
gulp.task('build', gulp.series(
    'clean',
    gulp.parallel(
        'style',
        'script',
        'img'
    ),
    'html'
))


//  DEV
gulp.task('dev', gulp.series(
    'build',
    'watch'
))
