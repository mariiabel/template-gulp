const gulp = require("gulp"),
    config = require('../gulpconfig.json')

gulp.task('watch', function() {
    console.log('\x1b[36m%s\x1b[0m', `START WATCH TASK `)
    browserSync.init({
        server: {
            baseDir: `${config.path.build}`,
            port: config.port
        },
        cors: true,
        notify: false,
        ui: false,
    });

    gulp.watch([
        `${config.path.source}/*.pug`,
        `${config.path.source}/data/*.json`,
        `${config.path.source}/templates/*.pug`
    ] , gulp.series('html'));

    gulp.watch([
        `${config.path.source}/components/**/*.scss`,
        `${config.path.source}/css/*.scss`
    ], gulp.series('style'));

    gulp.watch([
        `${config.path.source}/components/**/*.js`,
        `${config.path.source}/js/*.js`,
    ], gulp.series('script'));
    
    gulp.watch(
        `${config.path.source}/components/**/img/**/*.{jpg,png}`,
        gulp.parallel(
            'imgMin',
            'imgToWebp'
        ));

    gulp.watch(`${config.path.source}/img/sprite/*.svg`,  gulp.series('sprite'));
});

