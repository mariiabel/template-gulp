const gulp = require("gulp"),
    rename = require("gulp-rename"),
    babel = require("gulp-babel"),
    uglify = require('gulp-uglify'),
    rollup = require("gulp-better-rollup"),
    sourcemap = require("gulp-sourcemaps"),
    browserSync = require('browser-sync').create(),
    config = require('../gulpconfig.json')

gulp.task('script', function() {
    console.log('\x1b[36m%s\x1b[0m', `START JS TASK `)
    return gulp.src([
        `${config.path.source}/js`,
        `${config.path.source}/components/**/*.js`
        ], {
            base: `${config.path.source}`
        })
        .pipe(sourcemap.init())
        .pipe(rollup({
            input: `${config.path.source}/js/index.js`
        }, 'umd'))
        .pipe(babel())
        .pipe(gulp.dest(`${config.path.build}/js`))
        .pipe(uglify({ toplevel: true }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemap.write('.'))
        .pipe(gulp.dest(`${config.path.build}/js`))
        .pipe(browserSync.stream());
});

