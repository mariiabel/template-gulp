const gulp = require("gulp"),
    pug = require("gulp-pug"),
    dataG = require("gulp-data"),
    merge = require("gulp-merge-json"),
    path = require("path"),
    fs = require("fs"),
    browserSync = require('browser-sync').create(),
    config = require('../gulpconfig.json')

// DATA
gulp.task('jsonData', () => {
    console.log('\x1b[36m%s\x1b[0m', `START DATA TASK `)
    return gulp.src(`${config.path.source}/data/*.json`)
    .pipe(merge({
        fileName: 'data.json',
        edit: (json, file) => {
            let filename = path.basename(file.path, `.json`),
                primaryKey = filename.toUpperCase();
                dataFile = {};

            // Set the filename as the primary key for our JSON data
            dataFile[primaryKey] = json;

            return dataFile;
        }
    }))
    .pipe(gulp.dest(`${config.path.build}/data`));
})

// PUG
gulp.task('pug', () => {
    console.log('\x1b[36m%s\x1b[0m', `START HTML TASK `)
    return gulp.src([
            `${config.path.source}/*.pug`,
            // `${config.path.source}/components/**/_?*.pug`
        ], {
            base: `${config.path.source}`
        })
        .pipe(dataG(file => JSON.parse(fs.readFileSync(`${config.path.build}/data/data.json`))))
        .pipe(pug({
            // pretty: '   '
        }))
        .pipe(gulp.dest(`${config.path.build}`))
        .pipe(browserSync.stream());
})



gulp.task('html', gulp.series(
    'jsonData',
    'pug',
))

