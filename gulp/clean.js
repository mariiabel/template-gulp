const gulp = require("gulp"),
    del = require("del"),
    config = require('../gulpconfig.json')


// CLEAN
gulp.task('clean', function() {
    console.log('\x1b[36m%s\x1b[0m',`DELETE FOLDER: ${config.path.build}`)
    return del(`${config.path.build}`);
});

