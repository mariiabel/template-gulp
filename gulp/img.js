const gulp = require("gulp"),
    svgmin = require("gulp-svgmin"),
    svgstore = require("gulp-svgstore"),
    rename = require("gulp-rename"),
    imagemin = require("gulp-imagemin"),
    webp = require("gulp-webp"),
    browserSync = require('browser-sync').create(),
    config = require('../gulpconfig.json')


gulp.task('sprite', function() {
    console.log('\x1b[36m%s\x1b[0m', `START SPRITE TASK `)

    return gulp.src(
        `${config.path.source}/img/sprite/*.svg`
        , {
            base: `${config.path.source}/img/sprite`
        })
        .pipe(
            svgmin({
                plugins: [{
                    removeViewBox: false
                }]
            }))
        .pipe(svgstore())
        .pipe(rename("sprite.svg"))
        .pipe(gulp.dest(`${config.path.build}/img`))
        .pipe(browserSync.stream());
});

gulp.task('imgToWebp', function() {
    console.log('\x1b[36m%s\x1b[0m', `START IMG TO WEBP TASK `)
    return gulp.src(
        `${config.path.source}/components/**/img/**/*.{jpg,png}`
        , {
            base: `${config.path.source}/components`
        })
        .pipe(webp({
            quality: 80
        }))
        .pipe(gulp.dest(`${config.path.build}/img`))
})

gulp.task('imgMin', function() {
    console.log('\x1b[36m%s\x1b[0m', `START IMG MIN TASK `)
    return gulp.src(
        `${config.path.source}/components/**/img/**/*.{jpg,png}`
        , {
            base: `${config.path.source}/components`
        })
        .pipe(imagemin([
            imagemin.optipng({
                optimizationLevel: 3
            }),
            imagemin.mozjpeg({
                progressive: true
            }),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest(`${config.path.build}/img`))
        .pipe(browserSync.stream());
})

gulp.task('img', gulp.parallel(
    'sprite',
    'imgMin',
    'imgToWebp'
))
