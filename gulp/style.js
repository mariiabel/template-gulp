const gulp = require("gulp"),
    concat = require('gulp-concat');
    plumber = require("gulp-plumber"),
    sourcemap = require("gulp-sourcemaps"),
    sass = require('gulp-sass')(require('sass')),
    postcss = require('gulp-postcss'),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create(),
    config = require('../gulpconfig.json')

gulp.task('style',  () => {
    console.log('\x1b[36m%s\x1b[0m', `START CSS TASK `)
    return gulp.src([
            `${config.path.source}/css/index.scss`,
            `${config.path.source}/components/**/*.scss`
        ],
        {
            base: `${config.path.source}`
        })
        .pipe(concat('styles.scss'))
        .pipe(plumber())
        .pipe(sourcemap.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({ browsers: ['last 5 versions', '> 1%', 'ie 9', 'ie 8'], cascade: false }),
            cssnano()])
        )
        .pipe(rename({ suffix: ".min" }))
        .pipe(sourcemap.write("."))
        .pipe(gulp.dest(`${config.path.build}/css`))
        .pipe(browserSync.stream());
});