let links = document.querySelectorAll("[data-link]");

for (let link of links) {
    link.addEventListener('click', goToBlock)
}

function goToBlock(e) {
    e.preventDefault();
    let block = document.getElementById(this.dataset.link);
    block.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
}
function scrollTop(n) {
    window.scrollBy({
        top: n,
        behavior: 'smooth'
    })
}
